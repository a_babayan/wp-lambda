#Lambda base image Amazon linux
FROM public.ecr.aws/lambda/provided as builder
# Set desired PHP Version
ARG php_version="7.4.22"
RUN yum clean all && \
    yum install -y autoconf \
                bison \
                bzip2-devel \
                gcc \
                gcc-c++ \
                git \
                gzip \
                libcurl-devel \
                libxml2-devel \
                make \
                oniguruma \
                oniguruma-devel \
                openssl-devel \
                re2c\
                sqlite-devel \
                tar \
                unzip \
                zip

# Download the PHP source, compile, and install both PHP and Composer
RUN curl -sL https://github.com/php/php-src/archive/php-${php_version}.tar.gz | tar -xvz && \
    cd php-src-php-${php_version} && \
    ./buildconf --force && \
    ./configure --prefix=/opt/php-7-bin/ --with-openssl --with-curl --with-zlib --without-pear --enable-bcmath --with-bz2 --enable-mbstring --with-mysqli && \
    make -j 5 && \
    make install && \
    /opt/php-7-bin/bin/php -v && \
    curl -sS https://getcomposer.org/installer | /opt/php-7-bin/bin/php -- --install-dir=/opt/php-7-bin/bin/ --filename=composer

# Prepare runtime files
COPY runtime/bootstrap /lambda-php-runtime/
RUN chmod 0755 /lambda-php-runtime/bootstrap

# Install Guzzle, prepare vendor files
RUN mkdir /lambda-php-vendor && \
    cd /lambda-php-vendor && \
    /opt/php-7-bin/bin/php /opt/php-7-bin/bin/composer require guzzlehttp/guzzle


###### Create runtime image ######
FROM public.ecr.aws/lambda/provided as runtime
# Layer 1: PHP Binaries
COPY --from=builder /opt/php-7-bin /var/lang
# Layer 2: Shared libraries
COPY --from=builder /usr/lib64/libonig.so.2 /usr/lib64/libonig.so.2
# Layer 3: Runtime Interface Client
COPY --from=builder /lambda-php-runtime /var/runtime
# Layer 4: Vendor
COPY --from=builder /lambda-php-vendor/vendor /opt/vendor

RUN mkdir /wordpress

COPY src/ /var/task/
COPY wordpress/ /wordpress/

CMD [ "index" ]
